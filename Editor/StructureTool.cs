using UnityEditor;
using static UnityEditor.AssetDatabase;

namespace Vincent.Tools
{
    

    public static class StructureTool 
    {
        [MenuItem("Tools/Setup/Default Folders")]   
        public static void CreateDefaultFolders()
        {
            Folders.CreateDirectories("_Project", "Scripts", "Art", "Scenes", "Audio", "Shaders", "Prefabs", "Materials");
            Refresh();
        }

        [MenuItem("Tools/Setup/Load Default Manifest")]
        static async void LoadNewManifest()
        {
            await Packages.ReplaceFileFromGist("15cd7f3e95d0b26558899d5cbadb8787");
        }
        
        [MenuItem("Tools/Packages/Install 2D Basics")]
        static void Add2DBasics()
        {
            Packages.InstallUnityPackage("2d.animation", "2d.pixel-perfect", "2d.psdimporter", "2d.sprite", "2d.spriteshape", "2d.tilemap", "cinemachine", "textmeshpro");
        }

        [MenuItem("Tools/Packages/Install 3D Basics")]
        static void Add3DBasics()
        {
            Packages.InstallUnityPackage("cinemachine", "probuilder", "postprocessing", "textmeshpro", "timeline");
        }
        
        [MenuItem("Tools/Packages/Install IDE Plugins")]
        static void AddIDEPlugins()
        {
            Packages.InstallUnityPackage("ide.rider", "ide.vscode", "ide.visualstudio");
        }
        
        [MenuItem("Tools/Packages/Install Input System")]
        static void AddInputSystem()
        {
            Packages.InstallUnityPackage("inputsystem");
        }
    }
}
