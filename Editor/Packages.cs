﻿using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using static UnityEngine.Application;

namespace Vincent.Tools
{
    public static class Packages
    {
        public static async Task ReplaceFileFromGist(string id, string user = "vinlasan")
        {
            string url = GetGistURL(id, user); 
            string contents = await GetContents(url);
            ReplacePackageFile(contents);
        }
        
        private static string GetGistURL(string id, string user = "vinlasan")
        {
            return $"https://gist.github.com/{user}/{id}";
        }

        private static async Task<string> GetContents(string url)
        {
            using (HttpClient client = new HttpClient())
            {
                var response = await client.GetAsync(url);
                string content = await response.Content.ReadAsStringAsync();
                return content;
            }
        }

        private static void ReplacePackageFile(string contents)
        {
            string existing = Path.Combine(dataPath, "../Packages.manifest.json");
            File.WriteAllText(existing, contents);
            UnityEditor.PackageManager.Client.Resolve();
        }

        public static void InstallUnityPackage(params string[] packageName)
        {
            foreach (string package in packageName)
            {
                UnityEditor.PackageManager.Client.Add($"com.unity.{package}");
            }
        }
    }
}