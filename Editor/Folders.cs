﻿using static System.IO.Path;
using static System.IO.Directory;
using static UnityEngine.Application;

namespace Vincent.Tools
{
    public static class Folders
    {
        public static void CreateDirectories(string root, params string[] directories)
        {
            string fullPath = Combine(dataPath, root);
            foreach (string dir in directories)
            {
                CreateDirectory(Combine(fullPath, dir));
            }
        }
    }
}